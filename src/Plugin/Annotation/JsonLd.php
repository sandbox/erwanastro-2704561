<?php

/*
 * This file is part of the Nemrod package.
 *
 * (c) Conjecto <contact@conjecto.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Drupal\nemrod\Plugin\Annotation;

use Drupal\Component\Annotation\Plugin;

/**
 * Serializer JsonLd annotation.
 *
 * @Annotation
 * @Target({"CLASS", "METHOD"})
 */
class JsonLd extends Plugin
{
    /**
     * @var string
     */
    public $frame = null;

    /**
     * @var array
     */
    public $options = array();
}
