<?php

/*
 * This file is part of the Nemrod package.
 *
 * (c) Conjecto <contact@conjecto.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Drupal\nemrod\Plugin\Manager;

use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Plugin\DefaultPluginManager;

/**
 * Class SubClassOfPluginManager
 * @package Drupal\nemrod\Plugin\Manager
 */
class SubClassOfPluginManager extends DefaultPluginManager
{
    /**
     * SubClassOfPluginManager constructor.
     * @param \Traversable $namespaces
     * @param CacheBackendInterface $cache_backend
     * @param ModuleHandlerInterface $module_handler
     */
    public function __construct(\Traversable $namespaces, CacheBackendInterface $cache_backend, ModuleHandlerInterface $module_handler)
    {
        $subdir = 'RdfResource';

        // The name of the annotation class that contains the plugin definition.
        $plugin_definition_annotation_name = 'Drupal\nemrod\Plugin\Annotation\SubClassOf';

        parent::__construct($subdir, $namespaces, $module_handler, null, $plugin_definition_annotation_name);

        $this->alterInfo('nemrod_subclassof_info');

        $this->setCacheBackend($cache_backend, 'nemrod_sublcassof');
    }

}
