<?php

/*
 * This file is part of the Nemrod package.
 *
 * (c) Conjecto <contact@conjecto.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Drupal\nemrod\ParamConverter;

use Conjecto\Nemrod\Manager;
use Drupal\Core\DependencyInjection\Container;
use \Drupal\Core\ParamConverter\ParamConverterInterface;
use EasyRdf\RdfNamespace;
use Symfony\Component\Routing\Route;

/**
 * Find a rdf resource with parameters
 * Class ResourceParamConverter.
 */
class ResourceParamConverter implements ParamConverterInterface
{
    /**
     * @var Container
     */
    protected $container;

    /**
     * ResourceParamConverter constructor.
     * @param Container $container
     */
    public function __construct(Container $container)
    {
        $this->container = $container;
    }

    /**
     * @param mixed $value
     * @param mixed $definition
     * @param string $name
     * @param array $defaults
     * @return mixed
     * @throws \Exception
     */
    public function convert($value, $definition, $name, array $defaults)
    {
        // get specific endpoint if specified
        $resourceManager = 'rm';
        // endpoint specify from a route
        if (isset($definition['endpoint']) && $definition['endpoint']) {
            $resourceManager = 'nemrod.resource_manager.' . $definition['endpoint'];
        }
        // endpoint specified from an url param
        else if (isset($defaults['endpoint']) && $defaults['endpoint']) {
            $resourceManager = 'nemrod.resource_manager.' . $defaults['endpoint'];
        }

        /** @var Manager $rm */
        $rm = $this->container->get($resourceManager);
        if ($rm) {
            $repository = null;
            $rdfType = null;
            // type specified from a route
            if (isset($definition['type']) && $definition['type'] && $definition['type'] !== "rest:Resource") {
                $rdfType = $definition['type'];
                $repository = $rm->getRepository($rdfType);
            }
            // type specified from an url param
            else if (isset($defaults['type']) && $defaults['type']) {
                $rdfType = $defaults['type'];
                $repository = $rm->getRepository($defaults['type']);
            }

            $value = RdfNamespace::expand(urldecode($value));
            if ($repository) {
                // expand uri if it is prefixed, usefull for uri as url parameters
                $resource = $repository->find($value);
            }
            else {
                $resource = $rm->find($value);
            }

            // if no resource found, check if there is a _null create option
            if (!$resource && isset($definition['_null']) && $definition['_null'] === 'create') {
                $resource = $rm->create($rdfType);
            }

            return $resource;
        }

        return null;
    }

    /**
     * @param mixed $definition
     * @param string $name
     * @param Route $route
     * @return bool
     * @throws \Exception
     */
    public function applies($definition, $name, Route $route)
    {
        if (empty($definition['type'])) {
            return false;
        }

        foreach ($this->container->get('nemrod.namespace_registry')->namespaces() as $prefix => $uri) {
            if (strstr($definition['type'], $prefix . ':')) {
                return true;
            }
        }

        return false;
    }
}
