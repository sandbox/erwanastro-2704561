<?php

namespace Drupal\nemrod\Form;

use Conjecto\Nemrod\Manager;
use Conjecto\Nemrod\Resource;
use Conjecto\Nemrod\ResourceManager\Repository;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Render\Element;
use Drupal\Core\Routing\RouteMatchInterface;
use EasyRdf\Literal;
use EasyRdf\RdfNamespace;

/**
 * Class ResourceForm
 * @package Drupal\devyn\Form
 * @see \Drupal\Core\Entity\EntityForm
 */
class ResourceForm extends FormBase implements ResourceFormInterface
{
    /**
     * The module handler service.
     *
     * @var \Drupal\Core\Extension\ModuleHandlerInterface
     */
    protected $moduleHandler;

    /**
     * The resource manager.
     *
     * @var Manager
     *
     * @deprecated in Drupal 8.0.0, will be removed before Drupal 9.0.0.
     */
    protected $resourceManager;

    /**
     * The resource repository.
     *
     * @var Repository
     */
    protected $repository;

    /**
     * The resource being used by this form.
     *
     * @var Resource
     */
    protected $resource;

    /**
     * @var ResourceFormValues
     *
     * Transform resource property values to form values and vice versa
     */
    protected $resourceFormValues;

    /**
     * {@inheritdoc}
     */
    public function getBaseFormId()
    {
        return 'resource_form';
    }

    /**
     * {@inheritdoc}
     */
    public function getFormId()
    {
        return 'resource-' . $this->getResourceTypeId(). '_form' ;
    }

    /**
     * {@inheritdoc}
     */
    public function buildForm(array $form, FormStateInterface $form_state)
    {
        // During the initial form build, add this form object to the form state and
        // allow for initial preparation before form building and processing.
        if (!$form_state->has('resource_form_initialized')) {
            $this->init($form_state);
        }

        // Retrieve the form array using the possibly updated resource in form state.
        $form = $this->form($form, $form_state);

        // Set values to form fields
        foreach ($form as $key => $properties) {
            if (RdfNamespace::expand($key) !== $key) {
                $getMethod = 'get';
                if (strstr($properties['#type'], 'resource')) {
                    $getMethod = 'all';
                }
                $value = $this->resource->$getMethod($key);
                if ($value && $value instanceof Literal) {
                    $this->resourceFormValues->getLiteralValue($value, $form[$key]);
                }
                else if ($properties['#type'] === 'resource' || $properties['#type'] === 'resource_select') {
                    $this->resourceFormValues->constructResourceForm($this->resource->getRm(), $value, $form[$key]);
                }
            }
        }

        // Retrieve and add the form actions array.
        $actions = $this->actionsElement($form, $form_state);
        if (!empty($actions)) {
            $form['actions'] = $actions;
        }

        return $form;
    }

    /**
     * {@inheritdoc}
     */
    protected function init(FormStateInterface $form_state)
    {
        // Flag that this form has been initialized.
        $form_state->set('resource_form_initialized', TRUE);

        // Prepare the resource to be presented in the resource form.
        $this->prepareResource();

        // Invoke the prepare form hooks.
        $this->prepareInvokeAll('resource_prepare_form', $form_state);
        $this->prepareInvokeAll($this->getResourceTypeId() . '_prepare_form', $form_state);
    }

    /**
     * {@inheritdoc}
     */
    public function form(array $form, FormStateInterface $form_state)
    {
        // Add #process and #after_build callbacks.
        $form['#process'][] = '::processForm';
        $form['#after_build'][] = '::afterBuild';

        return $form;
    }

    /**
     * {@inheritdoc}
     */
    public function processForm($element, FormStateInterface $form_state, $form)
    {
        // If the form is cached, process callbacks may not have a valid reference
        // to the resource object, hence we must restore it.
        $this->resource = $form_state->getFormObject()->getResource();

        return $element;
    }

    /**
     * {@inheritdoc}
     */
    public function afterBuild(array $element, FormStateInterface $form_state)
    {
        // Rebuild the resource if #after_build is being called as part of a form
        // rebuild, i.e. if we are processing input.
        if ($form_state->isProcessingInput()) {
            $this->buildResource($element, $form_state);
        }

        return $element;
    }

    /**
     * {@inheritdoc}
     */
    protected function actionsElement(array $form, FormStateInterface $form_state)
    {
        $element = $this->actions($form, $form_state);

        if (isset($element['delete'])) {
            // Move the delete action as last one, unless weights are explicitly provided
            $delete = $element['delete'];
            unset($element['delete']);
            $element['delete'] = $delete;
            $element['delete']['#button_type'] = 'danger';
        }

        if (isset($element['submit'])) {
            // Give the primary submit button a #button_type of primary.
            $element['submit']['#button_type'] = 'primary';
        }

        $count = 0;
        foreach (Element::children($element) as $action) {
            $element[$action] += array(
                '#weight' => ++$count * 5,
            );
        }

        if (!empty($element)) {
            $element['#type'] = 'actions';
        }

        return $element;
    }

    /**
     * Returns an array of supported actions for the current resource form.
     * @param array $form
     * @param FormStateInterface $form_state
     * @return mixed
     */
    protected function actions(array $form, FormStateInterface $form_state)
    {
        $actions['submit'] = array(
            '#type' => 'submit',
            '#value' => $this->t('Save'),
            '#submit' => array('::submitForm', '::save'),
        );

        return $actions;
    }

    /**
     * {@inheritdoc}
     */
    public function submitForm(array &$form, FormStateInterface $form_state)
    {
        $form_state->cleanValues();
        $this->buildResource($form, $form_state);
    }

    /**
     * {@inheritdoc}
     */
    public function save(array $form, FormStateInterface $form_state)
    {
        if ($this->resource->isBNode()) {
            $this->resourceManager->persist($this->resource);
        }
        $this->resourceManager->flush();
    }

    /**
     * {@inheritdoc}
     */
    public function buildResource(array $form, FormStateInterface $form_state)
    {
        $this->copyFormValuesToResource($form, $form_state);

        // Invoke all specified builders for copying form values to resource properties.
        if (isset($form['#resource_builders'])) {
            foreach ($form['#resource_builders'] as $function) {
                call_user_func_array($function, array($this->getResourceTypeId(), $this->resource, &$form, &$form_state));
            }
        }
    }

    /**
     * {@inheritdoc}
     */
    protected function copyFormValuesToResource(array $form, FormStateInterface $form_state)
    {
        foreach ($form_state->getCompleteForm() as $key => $value) {
            if (isset($value['#value']) && $value['#value'] && RdfNamespace::expand($key) !== $key) {
                $value = $this->resourceFormValues->getValueFromForm($this->resource->getRm(), $value);
                if (is_array($value)) {
                    $this->resource->delete($key);
                    foreach ($value as $val) {
                        $this->resource->add($key, $val);
                    }
                }
                else {
                    $this->resource->set($key, $value);
                }
            }
        }
    }

    /**
     * {@inheritdoc}
     */
    public function getResource()
    {
        return $this->resource;
    }

    /**
     * {@inheritdoc}
     */
    public function setResource(Resource $resource)
    {
        $this->resource = $resource;
        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function getResourceFromRouteMatch(RouteMatchInterface $route_match, $resource_rdf_type)
    {
        foreach ($route_match->getParameters() as $parameter) {
            if ($parameter instanceof Resource) {
                if ($parameter->get('rdf:type')->getUri() === RdfNamespace::expand($resource_rdf_type)) {
                    return $parameter;
                }
            }
        }

        return null;
    }

    /**
     * Prepares the resource object before the form is built first.
     */
    protected function prepareResource() { }

    /**
     * Invokes the specified prepare hook variant.
     *
     * @param string $hook
     *   The hook variant name.
     * @param \Drupal\Core\Form\FormStateInterface $form_state
     *   The current state of the form.
     */
    protected function prepareInvokeAll($hook, FormStateInterface $form_state)
    {
        $implementations = $this->moduleHandler->getImplementations($hook);
        foreach ($implementations as $module) {
            $function = $module . '_' . $hook;
            if (function_exists($function)) {
                // Ensure we pass an updated translation object and form display at
                // each invocation, since they depend on form state which is alterable.
                $args = array($this->resource, &$form_state);
                call_user_func_array($function, $args);
            }
        }
    }

    /**
     * {@inheritdoc}
     */
    public function setModuleHandler(ModuleHandlerInterface $module_handler)
    {
        $this->moduleHandler = $module_handler;
        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function setResourceManager(Manager $resourceManager)
    {
        $this->resourceManager = $resourceManager;
        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function setRepository(Repository $repository)
    {
        $this->repository = $repository;
        return $this;
    }

    /**
     * @param ResourceFormValues $resourceFormValues
     */
    public function setResourceFormValues($resourceFormValues)
    {
        $this->resourceFormValues = $resourceFormValues;
    }

    protected function getResourceTypeId()
    {
        return str_replace(':', '-', $this->resource->type());
    }
}