<?php

namespace Drupal\nemrod\Form;


use Conjecto\Nemrod\Manager;
use Conjecto\Nemrod\QueryBuilder\NemrodQueryBuilderLoader;
use Conjecto\Nemrod\QueryBuilder\Query;
use Drupal\Core\Datetime\DrupalDateTime;
use Drupal\Core\Form\FormStateInterface;
use EasyRdf\Literal;
use EasyRdf\Literal\Date;
use EasyRdf\Literal\DateTime;
use Symfony\Component\Serializer\Exception\UnsupportedException;
use Symfony\Component\Validator\Exception\InvalidArgumentException;

class ResourceFormValues
{
    /**
     * Get resource value and transform it for form
     * Return a literal value with correct widget
     * @param Literal $literal
     * @param $element
     * @return string
     */
    public function getLiteralValue(Literal $literal, &$element)
    {
        switch ($literal->getDatatype()) {
            case 'xsd:date':
                $this->getLiteralDateValue($element, $literal);
                break;
            case 'xsd:dateTime':
                $this->getLiteralDateValue($element, $literal);
                break;
            default:
                $element['#default_value'] = $literal->getValue();
                break;
        }
    }

    /**
     * Construct a resource reference form field (select, checkbox and radio)
     * @param array $values
     * @param $element
     */
    public function constructResourceForm(Manager $rm, $values, &$element)
    {
        // check rdf:class
        if (!isset($element['#class'])) {
            throw new InvalidArgumentException("You have to specifiy a rdf type with key #class to use resource_select form.");
        }
        $queryBuilder = null;
        // get query builder from closure if exists
        if (isset($element['#query_builder'])) {
            $queryBuilder = $element['#query_builder'];
        }
        // construct query to get resources
        else {
            // base query
            $queryBuilder = $rm->getQueryBuilder()
                ->reset()
                ->construct("?s a " . $element['#class'])
                ->where("?s a " . $element['#class']);
            // use display label property if specified
            if (isset($element['#property'])) {
                $queryBuilder->addConstruct("?s " . $element['#property'] . " ?label");
                $queryBuilder->andWhere("?s " . $element['#property'] . " ?label");
                $queryBuilder->orderBy('?label');
            }
            // add general display properties in query
            else {
                $labelProperties = array('rdfs:label', 'skos:prefLabel', 'foaf:name', 'rss:title', 'dc:title', 'dc11:title');
                $arrayUnions = array();
                $i = 1;
                foreach ($labelProperties as $labelProperty) {
                    $queryBuilder->addConstruct("?s " . $labelProperty . " ?label$i");
                    $arrayUnions[] = "?s " . $labelProperty . " ?label$i";
                    $i++;
                }
                $queryBuilder->addUnion($arrayUnions);
                $queryBuilder->orderBy('?label1');
            }
        }
        // get resources
        $resources = (new NemrodQueryBuilderLoader($queryBuilder, $rm, $element['#class']))
            ->getResources(Query::HYDRATE_COLLECTION, ['rdf:type' => $element['#class']]
        );

        if (!isset($element['options'])) {
            $element['#options'] = array();
        }
        if (!isset($element['default_value'])) {
            $element['#default_value'] = array();
        }
        // add resource options
        foreach ($resources as $resource) {
            $label = isset($element['#property']) ? $resource->get($element['#property']) : $resource->label();
            if ($label instanceof Literal) {
                $label = $label->getValue();
            }
            $element['#options'][$resource->getUri()] = $label;
        }
        // add default values
        foreach ($values as $value) {
            if (method_exists($value, 'getUri')) {
                $element['#default_value'][] = $value->getUri();
            }
        }

        // radio or checkbox form type
        if ($element['#type'] === 'resource') {
            // checkbox
            if (isset($element['#multiple']) && $element['#multiple']) {
                $element['#type'] = 'checkboxes';
            }
            // radio
            else {
                $element['#type'] = 'radios';
                // we want only one default value and not an array
                $element['#default_value'] = $element['#default_value'] ? reset($element['#default_value']) : null;
            }
        }
        // Select form type
        else if ($element['#type'] === 'resource_select') {
            $element['#type'] = 'select';

            // need to add the # before key
            foreach ($element['#options'] as $key => $value) {
                unset($element['#options'][$key]);
                $element['#options']['#' . $key] = $value;
            }

            foreach ($element['#default_value'] as $key => $value) {
                unset($element['#default_value'][$key]);
                $element['#default_value'][] = '#' . $value;
            }

            if (!isset($element['#multiple']) || !$element['#multiple']) {
                $element['#default_value'] = $element['#default_value'] ? reset($element['#default_value']) : null;
            }
        }
    }

    /**
     * Return value from form
     * @param Manager $rm
     * @param array $formValue
     * @return mixed
     */
    public function getValueFromForm(Manager $rm, $formValue)
    {
        switch ($formValue['#type']) {
            case 'select':
                $value = $this->getResourceReferenceValues($rm, $formValue);
                break;
            case 'checkboxes':
                $value = $this->getResourceReferenceValues($rm, $formValue);
                break;
            case 'radios':
                $value = $this->getResourceReferenceValues($rm, $formValue);
                break;
            case 'checkbox':
                $value = (bool)$formValue['#value'];
                break;
            case 'widget':
                $value = $formValue['widget'][0]['value']['#value']['object'];
                break;
            case 'container':
                $value = $formValue['widget'][0]['value']['#value']['object'];
                break;
            default:
                $value = $formValue['#value'];
                break;
        }

        if (is_object($value)) {
            switch (get_class($value)) {
                case 'Drupal\Core\Datetime\DrupalDateTime':
                    switch ($formValue['#type']) {
                        case 'widget':
                            $value = new DateTime($value->format('Y-m-d H:i:s'));
                            break;
                        case 'container':
                            $value = new Date($value->format('Y-m-d'));
                            break;
                        default:
                            throw new UnsupportedException(get_class($value) . ' is not supported with type ' . $formValue['#type']);
                    }
                    break;
                default:
                    throw new UnsupportedException(get_class($value) . ' is not supported');
            }
        }

        return $value;
    }

    /**
     * @param Manager $rm
     * @param $formValue
     * @return array
     */
    protected function getResourceReferenceValues(Manager $rm, $formValue)
    {
        $values = $formValue['#value'];
        $arrayValues = array();
        if (!is_array($values)) {
            $values = array($values);
        }
        foreach ($values as $value) {
            // check if value is an uri and we know the resource
            if ($value[0] === '#') {
                $value = substr($value, 1);
            }
            $resource = $rm->getUnitOfWork()->retrieveResource($value);
            if ($resource) {
                $arrayValues[] = $resource;
            }
            // it is not a managed resource
            else {
                $arrayValues[] = $value;
            }
        }
        return $arrayValues;
    }

    /**
     * Transform EasyRdf date to Drupal date widget
     * @param $element
     * @param Literal $literal
     * @return string
     */
    protected function getLiteralDateValue(&$element, Literal $literal)
    {
        switch ($element['#type']) {
            case 'date':
                $elementComplements = [
                    '#type' => 'container',
                    'widget' => [
                        [
                            'value' => [
                                '#type' => 'datetime',
                                '#default_value' => DrupalDateTime::createFromDateTime($literal->getValue()),
                                '#date_time_element' => 'none'
                            ]
                        ]
                    ]
                ];
                $element = array_merge($element, $elementComplements);
                break;
            case 'datetime':
                $elementComplements = [
                    '#type' => 'widget',
                    'widget' => [
                        [
                            'value' => [
                                '#type' => 'datetime',
                                '#default_value' => DrupalDateTime::createFromDateTime($literal->getValue())
                            ]
                        ]
                    ]
                ];
                $element = array_merge($element, $elementComplements);
                break;
            default:
                drupal_set_message($element['#type'] . " has not been interpreted", 'warning');
                break;
        }
    }
}