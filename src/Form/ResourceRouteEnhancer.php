<?php

namespace Drupal\nemrod\Form;

use Drupal\Core\Routing\Enhancer\RouteEnhancerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Route;
use Symfony\Cmf\Component\Routing\RouteObjectInterface;

/**
 * Class ResourceRouteEnhancer
 * @package Drupal\nemrod\Form
 * @see EntityRouteEnhancer
 */
class ResourceRouteEnhancer implements RouteEnhancerInterface
{
    /**
     * {@inheritdoc}
     */
    public function enhance(array $defaults, Request $request)
    {
        if (empty($defaults['_controller'])) {
            if (!empty($defaults['_resource_form'])) {
                $defaults = $this->enhanceEntityForm($defaults, $request);
            }
        }
        return $defaults;
    }

    /**
     * {@inheritdoc}
     */
    public function applies(Route $route)
    {
        return !$route->hasDefault('_controller') && $route->hasDefault('_resource_form');
    }

    /**
     * Update defaults for entity forms.
     *
     * @param array $defaults
     *   The defaults to modify.
     * @param \Symfony\Component\HttpFoundation\Request $request
     *   The Request instance.
     *
     * @return array
     *   The modified defaults.
     */
    protected function enhanceEntityForm(array $defaults, Request $request)
    {
        $defaults['_controller'] = 'controller.resource_form:getContentResult';

        return $defaults;
    }
}