<?php

namespace Drupal\nemrod\Form;

use Conjecto\Nemrod\Manager;
use Conjecto\Nemrod\Resource;
use Drupal\Core\Controller\ControllerResolverInterface;
use Drupal\Core\Controller\FormController;
use Drupal\Core\DependencyInjection\ClassResolverInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Form\FormBuilderInterface;
use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\devyn\RdfResource\Folder;
use EasyRdf\RdfNamespace;

/**
 * Wrapping controller for resource forms
 */
class ResourceFormController extends FormController
{
    /**
     * The module handler to invoke the alter hook.
     *
     * @var \Drupal\Core\Extension\ModuleHandlerInterface
     */
    protected $moduleHandler;

    /**
     * The class resolver.
     *
     * @var \Drupal\Core\DependencyInjection\ClassResolverInterface
     */
    protected $classResolver;

    /**
     * Constructs a new \Drupal\Core\Routing\Enhancer\FormEnhancer object.
     *
     * @param \Drupal\Core\Controller\ControllerResolverInterface $resolver
     *   The controller resolver.
     * @param \Drupal\Core\Form\FormBuilderInterface $form_builder
     *   The form builder.
     * @param ModuleHandlerInterface $moduleHandler
     *   The resource manager.
     * @param ClassResolverInterface $classResolver
     *   The class resolver.
     */
    public function __construct(ControllerResolverInterface $resolver, FormBuilderInterface $form_builder,
        ModuleHandlerInterface $moduleHandler, ClassResolverInterface $classResolver)
    {
        $this->moduleHandler = $moduleHandler;
        $this->classResolver = $classResolver;

        parent::__construct($resolver, $form_builder);
    }

    /**
     * {@inheritdoc}
     */
    protected function getFormArgument(RouteMatchInterface $route_match)
    {
        return $route_match->getRouteObject()->getDefault('_resource_form');
    }

    /**
     * {@inheritdoc}
     */
    protected function getFormObject(RouteMatchInterface $route_match, $formClass)
    {
        $resource_rdf_type = null;
        // get rdf:type of resource param
        foreach ($route_match->getParameters() as $parameter) {
            if ($parameter instanceof Resource) {
                $resource_rdf_type = RdfNamespace::shorten($parameter->get('rdf:type')->getUri());
                if ($resource_rdf_type) {
                    break;
                }
            }
        }

        $form_object = $this->classResolver->getInstanceFromDefinition($formClass);
        $resource = $form_object->getResourceFromRouteMatch($route_match, $resource_rdf_type);

        $form_object->setModuleHandler($this->moduleHandler);
        $form_object->setResourceManager($resource->getRm());
        $form_object->setRepository($resource->getRm()->getRepository($resource_rdf_type));
        $form_object->setResource($resource);
        $form_object->setResourceFormValues(new ResourceFormValues());

        return $form_object;
    }
}