<?php

namespace Drupal\nemrod\Form;

use Conjecto\Nemrod\Manager;
use Conjecto\Nemrod\Resource;
use Conjecto\Nemrod\ResourceManager\Repository;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Form\BaseFormIdInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\Core\StringTranslation\TranslationInterface;

/**
 * Interface ResourceFormInterface
 * @package Drupal\nemrod\Form
 * @see \EntityFormInterface\EntityFormInterface
 */
interface ResourceFormInterface extends BaseFormIdInterface
{
    /**
     * Gets the form resource.
     *
     * The form resource which has been used for populating form element defaults.
     *
     * @return \Drupal\Nemrod\Resource
     *   The current form resource.
     */
    public function getResource();

    /**
     * Sets the form resource.
     *
     * Sets the form resource which will be used for populating form element
     * defaults. Usually, the form resource gets updated by
     * \Drupal\Core\Entity\EntityFormInterface::submit(), however this may
     * be used to completely exchange the form resource, e.g. when preparing the
     * rebuild of a multi-step form.
     *
     * @param \Drupal\Nemrod\Resource $resource
     *   The resource the current form should operate upon.
     *
     * @return $this
     */
    public function setResource(Resource $resource);

    /**
     * Determines which resource will be used by this form from a RouteMatch object.
     *
     * @param \Drupal\Core\Routing\RouteMatchInterface $route_match
     *   The route match.
     * @param string $resource_type_id
     *   The resource type identifier.
     *
     * @return \Drupal\Nemrod\Resource
     *   The resource object as determined from the passed-in route match.
     */
    public function getResourceFromRouteMatch(RouteMatchInterface $route_match, $resource_type_id);

    /**
     * Builds an updated resource object based upon the submitted form values.
     *
     * For building the updated resource object the form's resource is cloned and
     * the submitted form values are copied to resource properties. The form's
     * resource remains unchanged.
     *
     * @param array $form
     *   A nested array form elements comprising the form.
     * @param \Drupal\Core\Form\FormStateInterface $form_state
     *   The current state of the form.
     *
     * @return \Drupal\Nemrod\Resource
     *   An updated copy of the form's resource object.
     */
    public function buildResource(array $form, FormStateInterface $form_state);

    /**
     * Form submission handler for the 'save' action.
     *
     * Normally this method should be overridden to provide specific messages to
     * the user and redirect the form after the resource has been saved.
     *
     * @param array $form
     *   An associative array containing the structure of the form.
     * @param \Drupal\Core\Form\FormStateInterface $form_state
     *   The current state of the form.
     *
     * @return int
     *   Either SAVED_NEW or SAVED_UPDATED, depending on the operation performed.
     */
    public function save(array $form, FormStateInterface $form_state);

    /**
     * Sets the module handler for this form.
     *
     * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
     *   The module handler.
     *
     * @return $this
     */
    public function setModuleHandler(ModuleHandlerInterface $module_handler);

    /**
     * Sets the resource manager for this form.
     *
     * @param Manager $resource_manager
     *   The resource manager.
     *
     * @return $this
     *
     * @deprecated in Drupal 8.0.0, will be removed before Drupal 9.0.0.
     *
     * @todo Remove this set call in https://www.drupal.org/node/2603542.
     */
    public function setResourceManager(Manager $resource_manager);

    /**
     * Sets the resource type manager for this form.
     *
     * @param Repository $repository
     *   The resource type repository.
     *
     * @return $this
     */
    public function setRepository(Repository $repository);
}