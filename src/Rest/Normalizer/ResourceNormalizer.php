<?php

/*
 * This file is part of the Nemrod package.
 *
 * (c) Conjecto <contact@conjecto.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Drupal\nemrod\Rest\Normalizer;

use Drupal\serialization\Normalizer\NormalizerBase;
use EasyRdf\Literal;
use EasyRdf\RdfNamespace;
use Symfony\Component\HttpKernel\Exception\UnsupportedMediaTypeHttpException;
use Symfony\Component\Serializer\Normalizer\DenormalizerInterface;

/**
 * Class ResourceNormalizer
 * @package Drupal\nemrod\Rest\Normalizer
 */
class ResourceNormalizer extends NormalizerBase implements DenormalizerInterface
{
    /**
     * @var mixed
     */
    protected $supportedInterfaceOrClass;

    /**
     * ResourceNormalizer constructor.
     */
    public function __construct()
    {
        $this->supportedInterfaceOrClass = \Drupal::getContainer()->getParameter('nemrod.resource.class');
    }

    /**
     * @param mixed $data
     * @param null $format
     * @return bool
     */
    public function supportsNormalization($data, $format = NULL)
    {
        return ($data === null || parent::supportsNormalization($data, $format));
    }

    /**
     * @param object $object
     * @param null $format
     * @param array $context
     * @return object
     */
    public function normalize($object, $format = null, array $context = array())
    {
        return $object;
    }

    /**
     * @param mixed $data
     * @param string $type
     * @param null $format
     * @return bool
     */
    public function supportsDenormalization($data, $type, $format = NULL)
    {
        return ($type === null || parent::supportsDenormalization($data, $type, $format));
    }

    /**
     * @param mixed $data
     * @param string $resource
     * @param null $format
     * @param array $context
     * @return string
     */
    public function denormalize($data, $resource, $format = null, array $context = array())
    {
        $rm = \Drupal::service('rm');
        $mapBNodeIds = array();
        $uriResource = null;
        // create if POST route method
        if (!$resource) {
            if (isset($context['canCreate']) && $context['canCreate']) {
                $resource = $rm->create($context['rdf:type']);
                $uriResource = $rm->persist($resource);
                $mapBNodeIds['_:b0'] = $resource;
            }
            else {
                throw new UnsupportedMediaTypeHttpException('No resource found, not a POST request');
            }
        }

        // first create and get all resources
        foreach ($data as $uri => $resourceDatas) {
            if ($uri !== $resource->getUri() && !($uri === '_:b0' && isset($context['canCreate']) && $context['canCreate'])) {
                $subject = null;
                // if not a bnode
                if (!substr($uri, 0, 2) == '_:') {
                    $subject = $rm->find($uri);
                }
                if (!$subject) {
                    $types = array();
                    foreach ($resourceDatas as $resourceData) {
                        if (isset($resourceData['rdf:type'])) {
                            $types[] = RdfNamespace::expand($resourceData['rdf:type']['value']);
                        }
                    }
                    // by default, we create a new resource with first rdf:type declared in the frame
                    $mostAccurateType = reset($types);

                    // try to find the real most accurate rdf:type
                    $fialiationBuilder = \Drupal::service('nemrod.filiation.builder');
                    $mostAccurateTypes = $fialiationBuilder->getMostAccurateType($types);
                    if ($mostAccurateTypes !== null && count($mostAccurateTypes) > 0) {
                        $mostAccurateType = reset($mostAccurateTypes);
                    }
                    $subject = $rm->create($mostAccurateType);
                }
                $mapBNodeIds[$uri] = $subject;
            }
            else {
                $mapBNodeIds[$uri] = $resource;
            }
        }

        // set triplets to resources
        foreach ($data as $uri => $resourceDatas) {
            $subject = $mapBNodeIds[$uri];
            $addTripletMethod = 'set';
            $previousPredicate = null;
            foreach ($resourceDatas as $resourceData) {
                foreach ($resourceData as $predicate => $object) {
                    if ($previousPredicate && $previousPredicate === $predicate) {
                        $addTripletMethod = 'add';
                    }
                    if ($object['type'] === 'bnode' || $object['type'] === 'uri') {
                        $subject->set($predicate, $mapBNodeIds[$object['value']]);
                    }
                    else {
                        if (RdfNamespace::expand($predicate) !== "http://www.w3.org/1999/02/22-rdf-syntax-ns#type") {
                            $literal = new Literal($object['value']);
                            $subject->$addTripletMethod($predicate, $literal);
                        }
                        else {
                            $addTypeMethod = $addTripletMethod . 'Type';
                            $subject->$addTypeMethod($object['value']);
                        }
                    }
                    $previousPredicate = $predicate;
                    $addTripletMethod = 'set';
                }
            }
        }

        $rm->flush();

        if ($uriResource) {
            $resource = $rm->find($uriResource, $context['rdf:type']);
        }

        return $resource;
    }
}