<?php

/*
 * This file is part of the Nemrod package.
 *
 * (c) Conjecto <contact@conjecto.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Drupal\nemrod\Rest\Routing;

use Drupal\Core\Routing\RouteSubscriberBase;
use Symfony\Component\Routing\Route;
use Symfony\Component\Routing\RouteCollection;

/**
 * Add basic_auth authentification for rest resources routes
 * Class ResourceRoutes
 * @package Drupal\nemrod\Routing
 */
class ResourceRoutes extends RouteSubscriberBase
{
    /**
     * @param RouteCollection $collection
     */
    protected function alterRoutes(RouteCollection $collection)
    {
        foreach (array('rest.resource.DELETE.jsonld', 'rest.resource.GET.jsonld', 'rest.resource.PATCH.jsonld', 'rest.resource.POST.jsonld') as $routeId) {
            /** @var Route $route */
            $route = $collection->get($routeId);
            $route->setOption('_auth', array('basic_auth'));
            $route->setRequirement('_user_is_logged_in', 'TRUE');
            $collection->add($routeId, $route);
        }
    }
}