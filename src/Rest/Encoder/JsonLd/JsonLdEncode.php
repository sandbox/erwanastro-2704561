<?php

/*
 * This file is part of the Nemrod package.
 *
 * (c) Conjecto <contact@conjecto.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Drupal\nemrod\Rest\Encoder\JsonLd;

use EasyRdf\Resource;
use Symfony\Component\HttpFoundation\File\Exception\UnexpectedTypeException;
use Symfony\Component\Serializer\Encoder\EncoderInterface;

/**
 * Class JsonLdEncode
 * @package Drupal\nemrod\Rest\Encoder\JsonLd
 */
class JsonLdEncode implements EncoderInterface
{
    /**
     * Encodes PHP data to a JSON string.
     *
     * {@inheritdoc}
     */
    public function encode($data, $format, array $context = array())
    {
        if (!$data) {
            return null;
        }

        if (!$data instanceof Resource) {
            throw new UnexpectedTypeException($data, 'EasyRdf or Nemrod Resource');
        }

        $frame = null;
        $serializer = \Drupal::service("nemrod.jsonld.serializer");
        $jsonLdManager = \Drupal::service("plugin.manager.jsonld");
        foreach ($jsonLdManager->getDefinitions() as $resource) {
            if ($resource['class'] === get_class($data)) {
                $frame = $resource['frame'];
            }
        }

        return $serializer->serialize($data, $frame);
    }

    /**
     * {@inheritdoc}
     */
    public function supportsEncoding($format)
    {
        return JsonLdEncoder::FORMAT === $format;
    }
}