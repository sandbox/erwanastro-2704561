<?php

/*
 * This file is part of the Nemrod package.
 *
 * (c) Conjecto <contact@conjecto.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Drupal\nemrod\Rest\Encoder\JsonLd;

use EasyRdf\Graph;
use EasyRdf\Parser;
use ML\IRI\IRI;
use ML\JsonLD\JsonLD;
use ML\JsonLD\LanguageTaggedString;
use Symfony\Component\Serializer\Encoder\DecoderInterface;

/**
 * Class JsonLdDecode
 * @package Drupal\nemrod\Rest\Encoder\JsonLd
 */
class JsonLdDecode implements DecoderInterface
{
    /** @var Graph $graph */
    protected $graph;

    /**
     * {@inheritdoc}
     */
    public function decode($data, $format, array $context = array())
    {
        $this->graph = new Graph();
        $arrayDatas = array();

        // use ml/jsonld parser to get quads
        $quads = JsonLD::toRdf($data);
        foreach ($quads as $quad) {
            // Ignore named graphs
            if (null !== $quad->getGraph()) {
                continue;
            }

            $subject = (string) $quad->getSubject();
            $predicate = (string) $quad->getProperty();

            if ($quad->getObject() instanceof IRI) {
                $object = array(
                    'type' => 'uri',
                    'value' => (string) $quad->getObject()
                );

                if ('_:' === substr($object['value'], 0, 2)) {
                    $object = array(
                        'type' => 'bnode',
                        'value' => $object['value']
                    );
                }
            } else {
                $object = array(
                    'type' => 'literal',
                    'value' => $quad->getObject()->getValue()
                );

                if ($quad->getObject() instanceof LanguageTaggedString) {
                    $object['lang'] = $quad->getObject()->getLanguage();
                } else {
                    $object['datatype'] = $quad->getObject()->getType();
                }
            }

            $arrayDatas[$subject][] = array(
                $predicate => $object
            );
        }

        return $arrayDatas;
    }

    /**
     * {@inheritdoc}
     */
    public function supportsDecoding($format)
    {
        return JsonLdEncoder::FORMAT === $format;
    }
}