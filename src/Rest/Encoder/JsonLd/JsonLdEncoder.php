<?php

/*
 * This file is part of the Nemrod package.
 *
 * (c) Conjecto <contact@conjecto.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Drupal\nemrod\Rest\Encoder\JsonLd;

use Symfony\Component\Serializer\Encoder\DecoderInterface;
use Symfony\Component\Serializer\Encoder\EncoderInterface;

/**
 * Class JsonLdEncoder
 * @package Drupal\nemrod\Rest\Encoder\JsonLd
 */
class JsonLdEncoder implements EncoderInterface, DecoderInterface
{
    const FORMAT = 'jsonld';

    /**
     * @var JsonLdEncode
     */
    protected $encodingImpl;

    /**
     * @var JsonLdDecode
     */
    protected $decodingImpl;

    /**
     * JsonLdEncoder constructor.
     * @param JsonLdEncode|null $encodingImpl
     * @param JsonLdDecode|null $decodingImpl
     */
    public function __construct(JsonLdEncode $encodingImpl = null, JsonLdDecode $decodingImpl = null)
    {
        $this->encodingImpl = $encodingImpl ?: new JsonLdEncode();
        $this->decodingImpl = $decodingImpl ?: new JsonLdDecode(true);
    }

    /**
     * {@inheritdoc}
     */
    public function supportsEncoding($format)
    {
        return $this->encodingImpl->supportsEncoding($format);
    }

    /**
     * {@inheritdoc}
     */
    public function encode($data, $format, array $context = array())
    {
        return $this->encodingImpl->encode($data, self::FORMAT, $context);
    }

    /**
     * {@inheritdoc}
     */
    public function supportsDecoding($format)
    {
        return $this->decodingImpl->supportsDecoding($format);
    }

    /**
     * {@inheritdoc}
     */
    public function decode($data, $format, array $context = array())
    {
        return $this->decodingImpl->decode($data, self::FORMAT, $context);
    }
}