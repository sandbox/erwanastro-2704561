<?php

/*
 * This file is part of the Nemrod package.
 *
 * (c) Conjecto <contact@conjecto.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Drupal\nemrod\Rest;

use Drupal\Core\Render\RenderContext;
use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\rest\RequestHandler;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\HttpKernel\Exception\UnsupportedMediaTypeHttpException;
use Symfony\Component\Serializer\Exception\InvalidArgumentException;
use Symfony\Component\Serializer\Exception\UnexpectedValueException;
use Drupal\rest\ResourceResponse;

/**
 * Class ResourceRequestHandler
 * @package Drupal\nemrod\Rest
 */
class ResourceRequestHandler extends RequestHandler
{
    /**
     * @param RouteMatchInterface $route_match
     * @param Request $request
     * @return ResourceResponse|Response
     */
    public function handle(RouteMatchInterface $route_match, Request $request)
    {
        // Check JsonLd format
        $format = $route_match->getRouteObject()->getRequirement('_format') ?: 'json';
        $resource = $request->attributes->get('uri');
        if ($format !== "jsonld") {
            throw new InvalidArgumentException("Only jsonld format is supported, $format given");
        }

        // Deserialize incoming data if available
        $serializer = $this->container->get('serializer');
        $received = $request->getContent();
        if (!empty($received) && (in_array('POST', $route_match->getRouteObject()->getMethods()) || in_array('PATCH', $route_match->getRouteObject()->getMethods()))) {
            try {
                $resource = $serializer->deserialize($received, $resource, $format, array(
                        'canCreate' => in_array('POST', $route_match->getRouteObject()->getMethods()),
                        'rdf:type' => $request->attributes->get('type'))
                );
            } catch (UnexpectedValueException $e) {
                $error['error'] = $e->getMessage();
                $content = $serializer->serialize($error, 'json');
                return new Response($content, 400, array('Content-Type' => $request->getMimeType('json')));
            }
        } // Delete resource if asked
        else if (in_array('DELETE', $route_match->getRouteObject()->getMethods()) && $resource) {
            \Drupal::service('rm')->remove($resource);
            \Drupal::service('rm')->flush();
            $resource = null;
        }

        // Serialize and cache response
        $response = new ResourceResponse($resource, 200);
        $context = new RenderContext();
        $output = $this->container->get('renderer')->executeInRenderContext($context, function () use ($serializer, $resource, $format) {
            return $serializer->serialize($resource, $format);
        });
        $response->setContent($output);
        if (!$context->isEmpty()) {
            $response->addCacheableDependency($context->pop());
        }

        $response->headers->set('Content-Type', $request->getMimeType($format));
        // Add rest settings config's cache tags.
        $response->addCacheableDependency($this->container->get('config.factory')->get('rest.settings'));

        return $response;
    }
}
