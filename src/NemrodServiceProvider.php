<?php

/*
 * This file is part of the Nemrod package.
 *
 * (c) Conjecto <contact@conjecto.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Drupal\nemrod;

use Drupal\Core\DependencyInjection\ContainerBuilder;
use Drupal\Core\DependencyInjection\ServiceProviderInterface;
use Drupal\Core\Extension\ModuleHandler;
use Drupal\nemrod\DependencyInjection\Compiler\EventListenerRegistrationPass;
use Symfony\Component\DependencyInjection\DefinitionDecorator;
use Symfony\Component\DependencyInjection\Reference;

/**
 * Class NemrodServiceProvider
 * @package Drupal\nemrod
 */
class NemrodServiceProvider implements ServiceProviderInterface
{
    /** @var  ContainerBuilder $container */
    protected $container;

    /**
     * @param ContainerBuilder $container
     */
    public function register(ContainerBuilder $container)
    {
        $this->container = $container;
        $handler = \Drupal::service('module_handler');

        // sparql endpoints declared in hook_nemrod_sparql_endpoints
        $endpoints = $handler->invokeAll('nemrod_sparql_endpoints');
        $this->registerSparqlClients($endpoints);

        // register managers linked to SPARQL endpoints
        $this->registerResourceManagers($endpoints);

        // rdf resource mapping
        $this->registerResourceMappings($container, $handler);

        // register jsonld frames paths
        $this->registerJsonLdFramePaths($container, $handler);

        $container->addCompilerPass(new EventListenerRegistrationPass());
    }

    /**
     * Register SPARQL clients
     * @param array $endpoints
     */
    protected function registerSparqlClients($endpoints)
    {
        foreach ($endpoints as $name => $endpoint) {
            $this->container
                ->setDefinition('nemrod.sparql.connection.'.$name, new DefinitionDecorator('nemrod.sparql.connection'))
                ->setArguments(array(
                    $endpoint['query_uri'],
                    isset($endpoint['update_uri']) ? $endpoint['update_uri'] : null,
                ));
            $this->container->setAlias('sparql.'.$name, 'nemrod.sparql.connection.' . $name);
            if (isset($endpoint['default']) && $endpoint['default']) {
                $this->container->setAlias('sparql', 'nemrod.sparql.connection.' . $name);
            }
        }
    }
    
    /**
     * Register resource managers (one per connection)
     * @param array $endpoints
     */
    protected function registerResourceManagers($endpoints)
    {
        foreach ($endpoints as $name => $endpoint) {
            //repository factory
            $this->container->setDefinition('nemrod.repository_factory.'.$name, new DefinitionDecorator('nemrod.repository_factory'));

            //persister
            $this->container->setDefinition('nemrod.persister.'.$name, new DefinitionDecorator('nemrod.persister'))
                ->setArguments(array($endpoint['query_uri']));

            $evd = $this->container->setDefinition('nemrod.resource_lifecycle_event_dispatcher.'.$name, new DefinitionDecorator('nemrod.resource_lifecycle_event_dispatcher'));
            $evd->addTag('nemrod.event_dispatcher', array('endpoint' => $name));

            $rm = $this->container->setDefinition('nemrod.resource_manager.'.$name, new DefinitionDecorator('nemrod.resource_manager'));
            $rm->setArguments(array(new Reference('nemrod.repository_factory.'.$name), $endpoint['query_uri']))
                //adding query builder
                ->addMethodCall('setClient', array(new Reference('nemrod.sparql.connection.'.$name)))
                //adding metadatfactory
                ->addMethodCall('setMetadataFactory', array(new Reference('nemrod.metadata_factory')))
                ->addMethodCall('setUriPatternStore', array(new Reference('nemrod.resource_uripattern_store')))
                ->addMethodCall('setCascadePropertyRegistry', array(new Reference('nemrod.cascade_property_registry')))
                //adding event dispatcher
                ->addMethodCall('setEventDispatcher', array(new Reference('nemrod.resource_lifecycle_event_dispatcher.'.$name)))
                ->addMethodCall('setNamespaceRegistry', array(new Reference('nemrod.namespace_registry')));

            //setting main alias
            if (isset($endpoint['default']) && $endpoint['default']) {
                $this->container->setAlias('rm', 'nemrod.resource_manager.' . $name);
            }
        }
    }

    /**
     * Parses active bundles for resources to map
     * @param ContainerBuilder $container
     */
    protected function registerResourceMappings(ContainerBuilder $container, ModuleHandler $handler)
    {
        // registering all annotation mappings.
        $typeMapper = $container->getDefinition('nemrod.type_mapper');
        //setting default resource
        $typeMapper->addMethodCall('setDefaultResourceClass', array($container->getParameter('nemrod.resource.class')));
    }

    /**
     * Register jsonld frames paths for each bundle
     * @return string
     */
    protected function registerJsonLdFramePaths(ContainerBuilder $container, ModuleHandler $handler)
    {
        $jsonLdFilesystemLoaderDefinition = $container->getDefinition('nemrod.jsonld.frame.loader.filesystem');
        foreach ($handler->getModuleList() as $module => $filename) {
            if (is_dir($dir = $filename->getPath() . '/src/frames')) {
                $jsonLdFilesystemLoaderDefinition->addMethodCall('addPath', array($dir, $module));
            }
        }

        $jsonLdFilesystemLoaderDefinition->addMethodCall('setFiliationBuilder', array(new Reference('nemrod.filiation.builder')));
        $jsonLdFilesystemLoaderDefinition->addMethodCall('setMetadataFactory', array(new Reference('nemrod.jsonld.metadata_factory')));
    }
}